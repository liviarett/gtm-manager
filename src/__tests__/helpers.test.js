import gtm from '../api/gtm';
import {
  createLegacyTrigger,
  createLegacyTag,
  createNewVersion,
  publishVersion,
  updateTriggers,
  updateGtmContainer,
  checkGtmContainer,
} from '../helpers/gtmContainer.helpers';
import {
  getPageViewAndCustomTriggers,
  getLegacyPageviewTrigger,
  getPageViewTriggers,
  getCustomTriggers,
} from '../helpers/triggers.helpers';
import triggersWithLegacy from '../stubs/triggersWithLegacy';
import triggersWithCustom from '../stubs/triggersWithCustom';
import triggersWithPageViewAndCustom from '../stubs/triggersWithPageViewAndCustom';
import triggersWithoutPageViewOrCustom from '../stubs/triggersWithoutPageViewOrCustom';
import tagsWithLegacy from '../stubs/tagsWithLegacy';
import tagsWithoutLegacy from '../stubs/tagsWithoutLegacy';
import versionWithUpdates from '../stubs/versionWithUpdates';
import container from '../stubs/containerWithPageView';

gtm.createTrigger = jest.fn(() => Promise.resolve({ triggerId: '1' }));
gtm.createTag = jest.fn(() => Promise.resolve({}));
gtm.getTags = jest.fn(() => Promise.resolve([]));
gtm.updateTrigger = jest.fn(() => Promise.resolve({ status: 200 }));
gtm.createNewVersion = jest.fn(() => Promise.resolve({ containerVersionId: '2', path: 'path/to/version/2'}));
gtm.getLatestVersion = jest.fn(() => Promise.resolve(versionWithUpdates(tagsWithLegacy('119'), triggersWithLegacy)));
gtm.getLiveVersion = jest.fn(() => Promise.resolve({}));
gtm.publishVersion = jest.fn(() => Promise.resolve('SUCCESS'));

describe('Helper Functions', () => {
  jest.useFakeTimers();
  setTimeout.mockImplementation((fn) => Promise.resolve(fn()));

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('Trigger helpers', () => {
    it('should find legacy pageview trigger', () => {
      expect(getLegacyPageviewTrigger(triggersWithLegacy)[0]).toEqual(triggersWithLegacy[5])
    });

    it('should handle finding pageview triggers, ignoring custom triggers and excluding legacy trigger', () => {
      expect(getPageViewTriggers(triggersWithPageViewAndCustom)).toEqual([triggersWithPageViewAndCustom[3]])
    });

    it('should handle finding custom triggers and ignoring pageview triggers', () => {
      expect(getCustomTriggers(triggersWithPageViewAndCustom)).toEqual([triggersWithPageViewAndCustom[4]])
    });

    it('should handle finding both pageview and custom triggers', () => {
      expect(getPageViewAndCustomTriggers(triggersWithPageViewAndCustom)).toEqual([
        triggersWithPageViewAndCustom[3], triggersWithPageViewAndCustom[4]
      ])
    });

    it('should handle matching both matchRegex and contains', () => {
      const triggers = {...triggersWithPageViewAndCustom};
      triggers[3].filter[1].type = 'contains';
      expect(getPageViewAndCustomTriggers(triggersWithPageViewAndCustom).length).toEqual(2);
    });

    it('should handle ignoring negate cases', () => {
      const triggers = { ...triggersWithPageViewAndCustom };
      triggers[3].filter[1].parameter.push({ type: 'boolean', key: 'negate', value: 'true' })
      expect(getPageViewAndCustomTriggers(triggersWithPageViewAndCustom).length).toEqual(1);
    });
  })

  describe('GTM Container Helpers', () => {
    it('should not try to create legacy trigger if it already exists', async (done) => {
      const gtmStatus = { hasLegacyTrigger: true };
      const result = await createLegacyTrigger(gtmStatus, triggersWithLegacy, 'path/to/trigger');

      expect(gtm.createTrigger).not.toHaveBeenCalled();
      expect(result).toEqual(triggersWithLegacy[5])
      done();
    });

    it('should not try to create legacy tag if it alreagy exists', async (done) => {
      const gtmStatus = { hasLegacyTag: true };
      const result = await createLegacyTag(gtmStatus, triggersWithLegacy[5], 'path/to/tag');

      expect(gtm.createTag).not.toHaveBeenCalled();
      expect(result).toEqual(undefined);
      done();
    });

    it('should not try to create legacy tag if there is no legacy trigger id', async (done) => {
      const result = createLegacyTag({}, { triggerId: undefined }, 'path/to/tag');
      await expect(result).rejects.toThrow();

      expect(gtm.createTag).not.toHaveBeenCalled();
      done();
    });

    it('should not try to update triggers if they have already been updated', async (done) => {
      const gtmStatus = { hasUpdatedCustomTriggers: true };
      const result = await updateTriggers(gtmStatus, triggersWithPageViewAndCustom);

      expect(gtm.updateTrigger).not.toHaveBeenCalled();
      expect(result).toEqual(undefined);
      done();
    });

    it('should not try to create new version if it has already been created', async (done) => {
      const gtmStatus = { hasVersion: true };
      const latestVersion = { containerVersionId: '2', path: 'path/to/version/2' };
      const result = await createNewVersion(gtmStatus, latestVersion, 'path/to/create/version');

      expect(gtm.createNewVersion).not.toHaveBeenCalled();
      expect(result).toEqual(latestVersion);
      done();
    });

    it('should not publish version if it has already been published', async (done) => {
      const gtmStatus = { hasBeenPublished: true };
      const latestVersion = { containerVersionId: '2', path: 'path/to/version/2' };
      const result = await publishVersion(gtmStatus, latestVersion);

      expect(gtm.publishVersion).not.toHaveBeenCalled();
      expect(result).toEqual(undefined);
      done();
    });

    it('should not publish version if no version is there is no version', async (done) => {
      const latestVersion = { containerVersionId: '2', path: undefined };
      const result = publishVersion({}, latestVersion);

      await expect(result).rejects.toThrow();
      expect(gtm.publishVersion).not.toHaveBeenCalled();
      done();
    });

    describe('When updating GTM Container', () => {
      it('should succeed updating gtmContainer', async (done) => {
        const result = await updateGtmContainer(container);

        expect(gtm.createTrigger).toHaveBeenCalledTimes(1);
        expect(gtm.createTag).toHaveBeenCalledTimes(1);
        expect(gtm.updateTrigger).toHaveBeenCalledTimes(2);
        expect(gtm.createNewVersion).toHaveBeenCalledTimes(1);
        expect(gtm.publishVersion).toHaveBeenCalledTimes(1);
        expect(result).toEqual('SUCCESS');
        done();
      });

      it('should retry updating trigger', async (done) => {
        gtm.updateTrigger.mockImplementationOnce(jest.fn(() => Promise.resolve({ status: 429 })))

        const result = await updateGtmContainer(container);

        expect(result).toEqual('SUCCESS');
        expect(gtm.updateTrigger).toHaveBeenCalledTimes(3);
        done();
      });

      it('should fail updating when legacy trigger has not been found', async (done) => {
        gtm.createTrigger.mockImplementationOnce(jest.fn(() => Promise.resolve({})));
        const result = await updateGtmContainer(container);

        expect(gtm.createTag).not.toHaveBeenCalled();
        expect(gtm.updateTrigger).not.toHaveBeenCalled();
        expect(gtm.createNewVersion).not.toHaveBeenCalled();
        expect(gtm.publishVersion).not.toHaveBeenCalled();
        expect(result).toEqual('FAILED');
        done();
      })

      it('should fail updating when new version has not been found', async (done) => {
        gtm.createNewVersion.mockImplementationOnce(jest.fn(() => Promise.resolve({})));
        const result = await updateGtmContainer(container);

        expect(gtm.publishVersion).not.toHaveBeenCalled();
        expect(result).toEqual('FAILED');
        done();
      })
    });

    describe('When checking GTM Container', () => {
      const getTriggers = jest.fn();
      const updateContainer = jest.fn();
      const setContainerUpdated = jest.fn();
      const updateContainerStatus = jest.fn();
      const updateContainerGtmStatus = jest.fn();

      const component = {
        props: {
          publicId: 'GTM-1234567',
          path: 'path/to/container/GTM-1234567',
          workspaces: [{
            path: 'path/to/workspace',
          }]
        },
        getTriggers: getTriggers,
        updateContainer: updateContainer,
        setContainerUpdated: setContainerUpdated,
        updateContainerStatus: updateContainerStatus,
        updateContainerGtmStatus: updateContainerGtmStatus,
      }

      it('should set container updated if it has no pageview and no custom triggers', async (done) => {
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithoutPageViewOrCustom));
        await checkGtmContainer(component);

        expect(getTriggers).toHaveBeenCalledTimes(1);
        expect(setContainerUpdated).toHaveBeenCalledTimes(1);
        done();
      });

      it('should know when gtm container has legacy trigger', async (done) => {
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithLegacy));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(2);
        expect(updateContainerGtmStatus).toHaveBeenNthCalledWith(1, 'hasLegacyTrigger', true);
        done();
      });

      it('should know when gtm container does not have legacy trigger', async (done) => {
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithPageViewAndCustom));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(1);
        expect(updateContainerGtmStatus).toHaveBeenNthCalledWith(1, 'hasLegacyTrigger', false);
        expect(updateContainer).toHaveBeenCalledWith(expect.objectContaining({ status: 'ATTENTION' }));
        expect(setContainerUpdated).not.toHaveBeenCalled();
        done();
      });

      it('should know when gtm container has legacy tag', async (done) => {
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithLegacy));
        gtm.getTags.mockImplementationOnce(() => Promise.resolve(tagsWithLegacy('119')));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(3);
        expect(updateContainerGtmStatus).toHaveBeenNthCalledWith(2, 'hasLegacyTag', true);
        done();
      });

      it('should know when gtm container has legacy tag', async (done) => {
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithLegacy));
        gtm.getTags.mockImplementationOnce(() => Promise.resolve(tagsWithoutLegacy));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(2);
        expect(updateContainerGtmStatus).toHaveBeenCalledWith('hasLegacyTag', false);
        expect(updateContainer).toHaveBeenCalledWith(expect.objectContaining({ status: 'ATTENTION' }));
        expect(setContainerUpdated).not.toHaveBeenCalled();
        done();
      });

      it('should know when gtm container has updated all pageview triggers', async (done) => {
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithCustom));
        gtm.getTags.mockImplementationOnce(() => Promise.resolve(tagsWithLegacy('119')));
        gtm.getLatestVersion.mockImplementationOnce(() => Promise.resolve({ tag: [], trigger: []}));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(4);
        expect(updateContainerGtmStatus).toHaveBeenNthCalledWith(3, 'hasUpdatedCustomTriggers', true);
        done();
      });

      it('should know when gtm container has not updated all pageview triggers', async (done) => {
        getTriggers.mockImplementationOnce(() => Promise.resolve([ ...triggersWithPageViewAndCustom, ...triggersWithLegacy ]));
        gtm.getTags.mockImplementationOnce(() => Promise.resolve(tagsWithLegacy('119')));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(3);
        expect(updateContainerGtmStatus).toHaveBeenLastCalledWith('hasUpdatedCustomTriggers', false);
        expect(updateContainer).toHaveBeenCalledWith(expect.objectContaining({ status: 'ATTENTION' }));
        expect(setContainerUpdated).not.toHaveBeenCalled();
        done();
      });

      it('should know when latest version has all updates', async (done) => {
        const tags = tagsWithLegacy('119');
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithCustom));
        gtm.getTags.mockImplementationOnce(() => Promise.resolve(tags));
        gtm.getLatestVersion.mockImplementationOnce(() => Promise.resolve(versionWithUpdates(tags, triggersWithCustom)));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(5);
        expect(updateContainerGtmStatus).toHaveBeenNthCalledWith(4, 'hasVersion', true);
        done();
      });

      it('should know when latest version does not have all updates', async (done) => {
        const tags = tagsWithLegacy('119');
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithCustom));
        gtm.getTags.mockImplementationOnce(() => Promise.resolve(tags));
        gtm.getLatestVersion.mockImplementationOnce(() => Promise.resolve(versionWithUpdates(tags, triggersWithPageViewAndCustom)));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(4);
        expect(updateContainerGtmStatus).toHaveBeenLastCalledWith('hasVersion', false);
        expect(updateContainer).toHaveBeenCalledWith(expect.objectContaining({ status: 'ATTENTION' }));
        expect(setContainerUpdated).not.toHaveBeenCalled();
        done();
      });

      it('should know when latest version has been published and set container updated', async (done) => {
        const tags = tagsWithLegacy('119');
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithCustom));
        gtm.getTags.mockImplementationOnce(() => Promise.resolve(tags));
        gtm.getLatestVersion.mockImplementationOnce(() => Promise.resolve(versionWithUpdates(tags, triggersWithCustom)));
        gtm.getLiveVersion.mockImplementationOnce(() => Promise.resolve(versionWithUpdates(tags, triggersWithCustom)));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(5);
        expect(updateContainerGtmStatus).toHaveBeenLastCalledWith('hasBeenPublished', true);
        expect(setContainerUpdated).toHaveBeenCalledTimes(1);
        done();
      });

      it('should know when latest version has not been published', async (done) => {
        const tags = tagsWithLegacy('119');
        getTriggers.mockImplementationOnce(() => Promise.resolve(triggersWithCustom));
        gtm.getTags.mockImplementationOnce(() => Promise.resolve(tags));
        gtm.getLatestVersion.mockImplementationOnce(() => Promise.resolve(versionWithUpdates(tags, triggersWithCustom)));
        gtm.getLiveVersion.mockImplementationOnce(() => Promise.resolve(versionWithUpdates(tags, triggersWithCustom, '50')));

        await checkGtmContainer(component);
        expect(updateContainerGtmStatus).toHaveBeenCalledTimes(5);
        expect(updateContainerGtmStatus).toHaveBeenLastCalledWith('hasBeenPublished', false);
        expect(updateContainer).toHaveBeenCalledWith(expect.objectContaining({ status: 'ATTENTION' }));
        expect(setContainerUpdated).not.toHaveBeenCalled();
        done();
      });
    })
  })
})