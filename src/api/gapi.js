import credentials from './credentials';

const apiKey = credentials.apiKey;
const clientId = credentials.clientId;
const discoveryDocs = [
  'https://www.googleapis.com/discovery/v1/apis/tagmanager/v2/rest',
  'https://people.googleapis.com/$discovery/rest?version=v1',
];
const scopes = ['profile',
  'https://www.googleapis.com/auth/userinfo.profile',
  'https://www.googleapis.com/auth/tagmanager.manage.accounts',
  'https://www.googleapis.com/auth/tagmanager.edit.containers',
  'https://www.googleapis.com/auth/tagmanager.delete.containers',
  'https://www.googleapis.com/auth/tagmanager.edit.containerversions',
  'https://www.googleapis.com/auth/tagmanager.manage.users',
  'https://www.googleapis.com/auth/tagmanager.publish'
];

const initGoogleApi = (callback) => {
  if (window.gapi) {
    window.gapi.load('client:auth2', initClient(callback));
  } else {
    setTimeout(() => {
      initGoogleApi(callback);
    }, 500);
  }
}

const initClient = (callback, attempts = 0) => {
  if (window.gapi.client) {
    window.gapi.client.init({
      apiKey: apiKey,
      discoveryDocs: discoveryDocs,
      clientId: clientId,
      scope: scopes.join(' ')
    }).then(() => {
      window.gapi.auth2.getAuthInstance().isSignedIn.listen(callback);
      callback(isSignedIn());
    });
  } else if (attempts < 4) {
    setTimeout(() => {
      initClient(callback, attempts++);
    }, 500)
  }
}

const signIn = () => window.gapi.auth2.getAuthInstance().signIn();
const signOut = () => window.gapi.auth2.getAuthInstance().signOut();

const isSignedIn = () =>
  window.gapi.auth2.getAuthInstance().isSignedIn.get();

const getUserInfo = (setUser, setGtmAccounts) => {
  window.gapi.client.people.people.get({
    'resourceName': 'people/me',
    'requestMask.includeField': 'person.names,person.metadata,person.photos'
  }).then(resp => {
    setUser({
    name: resp.result.names[0].displayName,
    photo: resp.result.photos[0].url,
  })});

  window.gapi.client.tagmanager.accounts.list()
    .then(res => {
      setGtmAccounts(res.result.account)
    });
};

export default {
  initGoogleApi,
  isSignedIn,
  getUserInfo,
  signIn,
  signOut
};