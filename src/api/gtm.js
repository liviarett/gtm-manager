import { customTriggerTemplate, versionTemplate } from './resources';

const getContainers = (accountPath, callback) =>
  window.gapi.client.tagmanager.accounts.containers.list({
    'parent': accountPath
  })
  .then(response => {
    callback(response.result.container);
  })

const getWorkspaces = (parent, callback) =>
  window.gapi.client.tagmanager.accounts.containers.workspaces.list({ parent })
    .then(response => {
      callback({ workspaces: response.result.workspace });
      return Promise.resolve(response.result.workspace);
    })

const getTags = (parent, callback) =>
  window.gapi.client.tagmanager.accounts.containers.workspaces.tags.list({ parent })
    .then(response => {
      const tags = response.result.tag;
      callback({ tags })
      return tags;
    })

const getTriggers = (parent, callback) =>
  window.gapi.client.tagmanager.accounts.containers.workspaces.triggers.list({ parent })
    .then(response => {
      const triggers = response.result.trigger;
      callback({ triggers, filteredTriggers: triggers })
      return triggers;
    })

const getLatestVersionPath = (parent) =>
  window.gapi.client.tagmanager.accounts.containers.version_headers.list({ parent })
    .then(response => response.result.containerVersionHeader.pop().path);

const getLatestVersion = (parent, callback) =>
  getLatestVersionPath(parent)
    .then(path => window.gapi.client.tagmanager.accounts.containers.versions.get({ path }))
    .then(response => {
      const latestVersion = response.result;
      callback({ latestVersion });
      return latestVersion;
    })

const getLiveVersion = (parent, callback) =>
  window.gapi.client.tagmanager.accounts.containers.versions.live({ parent })
    .then(response => {
      const liveVersion = response.result;
      callback({ liveVersion });
      return liveVersion;
    })

const createTrigger = (parent, resource) =>
  window.gapi.client.tagmanager.accounts.containers.workspaces.triggers.create({
    resource,
    parent
  })
    .then(response => response.result)
    .catch(() => createTrigger(parent, resource));

const createTag = (parent, resource) =>
  window.gapi.client.tagmanager.accounts.containers.workspaces.tags.create({ resource, parent })
    .then(response => response.result)
    .catch(() => createTag(parent, resource))

export const updateTrigger = (trigger) =>
  window.gapi.client.tagmanager.accounts.containers.workspaces.triggers.update({
    path: trigger.path,
    resource: {
      ...trigger,
      ...customTriggerTemplate,
    },
  })
    .then(response => response)

const createNewVersion = (path) =>
  window.gapi.client.tagmanager.accounts.containers.workspaces.create_version({
    path,
    resource: versionTemplate,
  })
    .then(response => response.result.containerVersion);

const publishVersion = (path) =>
  window.gapi.client.tagmanager.accounts.containers.versions.publish({ path })
    .then(response => response.status === 200 ? 'SUCCESS' : 'FAILED');

export default {
  getContainers,
  getWorkspaces,
  getTags,
  getTriggers,
  getLatestVersion,
  getLiveVersion,
  createTrigger,
  createTag,
  createNewVersion,
  updateTrigger,
  publishVersion,
}