export const legacyPageViewTriggerTemplate = {
  name: 'Legacy Conversion Page View Trigger',
  type: 'pageview',
  filter: [
    {
      parameter: [
        {
          type: 'template',
          key: 'arg0',
          value: '{{Page URL}}',
        },
        {
          type: 'template',
          key: 'arg1',
          value: 'sid=',
        },
        {
          type: 'boolean',
          key: 'ignore_case',
          value: 'true',
        }
      ],
      type: 'matchRegex',
    }
  ]
}

export const legacyPageViewHtmlTagTemplate = (firingTriggerId) => ({
  name: 'Legacy Conversion Page',
  type: 'html',
  parameter: [
    {
      type: 'template',
      key: 'html',
      value: '<script>dataLayer.push({ event: "webform_submission_completed" })</script>',
    },
    {
      type: 'boolean',
      key: 'supportDocumentWrite',
      value: 'true',
    },
  ],
  firingTriggerId: [firingTriggerId],
});

export const customTriggerTemplate = {
  type: 'customEvent',
  customEventFilter: [{
    type: 'equals',
    parameter: [{
      type: 'template',
      key: 'arg0',
      value: '{{_event}}'
    }, {
      type: 'template',
      key: 'arg1',
      value: 'webform_submission_completed'
    }]
  }],
}

export const versionTemplate = {
  name: 'Custom Conversion Events',
  notes: 'This version updates converion triggers to listen for the custom event webform_submission_completed and creates a backwards compatible html tag that injects that event on the legacy conversion page load',
}