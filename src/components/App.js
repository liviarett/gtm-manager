import React, { Component } from 'react';
import Auth from './Auth';
import Containers from './Containers';
import GtmAccounts from './GtmAccounts';
import Header from './Header';
import { getLocalGtmContainersStatus } from '../helpers/gtmContainer.helpers';

const initialState = {
  isSignedIn: false,
  containers: [],
  gtmAccounts: [],
  selectedGtmAccount: '',
  user: {},
}

class App extends Component {
  constructor() {
    super();
    this.state = initialState;
    this.updateSignInStatus = this.updateSignInStatus.bind(this);
    this.setContainers = this.setContainers.bind(this);
    this.setGtmAccounts = this.setGtmAccounts.bind(this);
    this.setSelectedGtmAccount = this.setSelectedGtmAccount.bind(this);
    this.clearState = this.clearState.bind(this);
    this.setUser = this.setUser.bind(this);
    this.updateContainer = this.updateContainer.bind(this);
  }

  clearState() {
    this.setState({
      ...initialState,
    })
  }

  setUser(user) {
    this.setState({
      user
    })
  }

  updateSignInStatus(isSignedIn) {
    this.setState({
      isSignedIn
    })
  }

  setContainers(containers) {
    const localGtmContainerStatus = getLocalGtmContainersStatus();

    const mappedContainers = containers.map(container => {
      const localContainerStatus = localGtmContainerStatus[container.publicId];
      if (localContainerStatus) {
        container.status = localContainerStatus;
      }
      return container;
    });

    this.setState({
      containers: mappedContainers
    })
  }

  setGtmAccounts(gtmAccounts) {
    this.setState({
      gtmAccounts
    })
  }

  setSelectedGtmAccount(selectedGtmAccount) {
    this.setState({
      selectedGtmAccount
    })
  }


  updateContainer(containerId) {
    return (options) => {
      const containers = this.state.containers.map(container => {
        if (container.containerId === containerId) {
          return {
            ...container,
            ...options,
          }
        }
        return container;
      });

      this.setState({
        containers,
      })
    }
  }

  render() {
    return (
      <div className="App">
        <Header>
          <Auth
            updateSignInStatus={this.updateSignInStatus}
            isSignedIn={this.state.isSignedIn}
            setGtmAccounts={this.setGtmAccounts}
            clearState={this.clearState}
            setUser={this.setUser}
            user={this.state.user}
          />
        </Header>
        <GtmAccounts
          gtmAccounts={this.state.gtmAccounts}
          selectedGtmAccount={this.state.selectedGtmAccount}
          setSelectedGtmAccount={this.setSelectedGtmAccount}
        />
        {this.state.selectedGtmAccount &&
          <Containers
            updateContainer={this.updateContainer}
            setContainers={this.setContainers}
            containers={this.state.containers}
            account={this.state.selectedGtmAccount}
          />
        }
      </div>
    );
  }
}

export default App;
