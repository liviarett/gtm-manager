import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import gapi from '../api/gapi';
import { SmallAvatar as Avatar } from './Avatar';

const StyledAuth = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  span {
    font-size: .8rem;
  }

  i {
    margin-left: 1em;

    &:hover {
      text-shadow: 0px 1px 2px rgba(0,0,0,0.5);
    }
  }

  .avatar {
    margin-left: 1em;
  }
`;

class Auth extends Component {
  constructor() {
    super();
    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleSignOut = this.handleSignOut.bind(this);
  }

  componentDidMount() {
    gapi.initGoogleApi(this.props.updateSignInStatus);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.isSignedIn !== this.props.isSignedIn && this.props.isSignedIn) {
      gapi.getUserInfo(this.props.setUser, this.props.setGtmAccounts);
    }
  }

  handleSignIn() {
    this.props.clearState();
    gapi.signIn();
  }

  handleSignOut() {
    this.props.clearState();
    gapi.signOut();
  }

  render() {
    return (
      <StyledAuth>
        {
          this.props.isSignedIn
            ?
            <Fragment>
              <span>{this.props.user.name}</span>
              <Avatar url={this.props.user.photo} />
              <i
                className="fas fa-sign-out-alt"
                onClick={this.handleSignOut}
                title="Log out from this account"
              >
              </i>
            </Fragment>
           :
          <span>
            <span>You are not signed in to Google.</span>
            <i onClick={this.handleSignIn} className="far fa-user"></i>
          </span>
        }
      </StyledAuth>
    );
  }
}

export default Auth;