import React from 'react';
import styled from 'styled-components';

const Avatar = ({ url, className }) => (
  <span className={`${className} avatar`}>
    <img src={url} alt="user" />
  </span>
);

export const SmallAvatar = styled(Avatar)`
  height: 33px;
  width: 33px;
  border-radius: 100% 100%;
  overflow: hidden;
  display: inline-block;

  img {
    max-height: 100%;
  }
`;

export default Avatar;