import React, { Fragment } from 'react';
import gtm from '../../api/gtm';
import {
  updateGtmContainer,
  checkGtmContainer,
} from '../../helpers/gtmContainer.helpers';
import {
  StyledGtmContainer,
} from '../styles';
import StatusIcon from '../StatusIcon';
import { setLocalGtmContainerStatus } from '../../helpers/gtmContainer.helpers';
import ContainerAction from './ContainerAction';
import Triggers from '../Triggers';
class Container extends React.Component {
  constructor(props) {
    super(props);
    this.getTriggers = this.getTriggers.bind(this);
    this.updateGtmContainer = this.updateGtmContainer.bind(this);
    this.updateContainer = this.updateContainer.bind(this);
    this.updateContainerStatus = this.updateContainerStatus.bind(this);
    this.updateContainerGtmStatus = this.updateContainerGtmStatus.bind(this);
    this.setContainerUpdated = this.setContainerUpdated.bind(this);
  }

  getTriggers() {
    return gtm.getWorkspaces(this.props.path, this.updateContainer)
      .then(workspaces =>
        gtm.getTriggers(workspaces[0].path, this.updateContainer)
      )
  }

  updateContainer(options) {
    return this.props.updateContainer(this.props.containerId)(options);
  }

  updateContainerStatus(status) {
    this.updateContainer({
      status,
    })
  }

  updateContainerGtmStatus(key, value) {
    this.updateContainer({
      gtmStatus : {
        ...this.props.gtmStatus,
        [key]: value,
      }
    })
  }

  setContainerUpdated() {
    this.updateContainerStatus('UPDATED');
    setLocalGtmContainerStatus(this.props.publicId, 'UPDATED');
  }

  async updateGtmContainer() {
    this.updateContainerStatus('UPDATING');

    const result = await updateGtmContainer(this.props, this.updateContainer);

    if (result === 'SUCCESS') {
      this.setContainerUpdated();
    } else {
      this.updateContainerStatus(result);
    }
  }

  render() {
    const {
      filteredTriggers,
      name,
      publicId,
      status,
    } = this.props;

    return (
      <div>
          <Fragment>
              <StyledGtmContainer onClick={this.getTriggers}>
                <span>{name}</span>
                <span>
                  <div>{publicId}</div>
                    <ContainerAction
                      status={status}
                      checkGtmContainer={() => checkGtmContainer(this)}
                      updateGtmContainer={this.updateGtmContainer}
                    />
                </span>
              <StatusIcon status={status}/>
              </StyledGtmContainer>
            {
              filteredTriggers && <Triggers triggers={filteredTriggers}/>
            }
        </Fragment>
      </div>
    );
  }
}

export default Container;