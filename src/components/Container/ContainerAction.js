import React from 'react';
import styled from 'styled-components';

const needsUpdating = (status) => status === 'ATTENTION';
const hasFailed = (status) => status === 'FAILED';

const Styled = styled.div`
  font-size: 11px;
  margin-top: .3em;
  display: flex;
  flex-direction: column;
`;

const ContainerAction = ({ status, checkGtmContainer, updateGtmContainer }) => (
  <Styled>
    {
      (!status || hasFailed(status)) && <span onClick={(e) => {
        e.stopPropagation();
        checkGtmContainer(this);
      }}>
        {hasFailed(status) ? 'retry' : 'check container'}
      </span>
    }
    {
      needsUpdating(status) && <span onClick={(e) => {
          e.stopPropagation();
          updateGtmContainer();
        }}>
          update container
        </span>
    }
  </Styled>
);

export default ContainerAction;