import React from 'react';
import { StyledGtmContainer } from '../styles';
import styled from 'styled-components';
import StatusIcon from '../StatusIcon';

const StyledUpdatedContainer = styled(StyledGtmContainer)`
  padding: 1em 2em;
  background-color: ${props => props.status === 'CHECKING' ? 'rgb(212, 225, 244)' : 'rgb(200, 246, 200)'};
`;

const UpdatedContainer = ({ name, publicId, status}) => (
  <StyledUpdatedContainer status={status}>
    <span>{name}</span>
    <span>{publicId}</span>
    <StatusIcon status={status} />
  </StyledUpdatedContainer>
)

export default UpdatedContainer;