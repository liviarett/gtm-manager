import React from 'react';
import gtm from '../api/gtm';
import Container from './Container/Container';
import UpdatedContainer from './Container/UpdatedContainer';
class Containers extends React.Component {
  constructor(props) {
    super(props);
    gtm.getContainers(this.props.account.path, this.props.setContainers);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.account.path !== this.props.account.path) {
      gtm.getContainers(this.props.account.path, this.props.setContainers);
    }
  }

  render() {

    return (
      <div>
        {this.props.containers.map(container =>
          container.status === 'UPDATED' ?
          <UpdatedContainer {...container} key={container.containerId} />
          :
          <Container {...container} updateContainer={this.props.updateContainer} key={container.containerId} />
        )}
      </div>
    );
  }
}

export default Containers;