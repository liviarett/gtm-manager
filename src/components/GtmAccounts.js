import React from 'react';
import styled from 'styled-components';

const GtmAccount = ({ accountId, name, path, isSelected, setSelectedGtmAccount }) => (
  <div
    className="gtm-account"
    style={{ fontWeight: isSelected ? '700' : '400' }}
    onClick={() => setSelectedGtmAccount({ name, accountId, path })}>{name}</div>
  );

const GtmAccounts = ({ gtmAccounts, selectedGtmAccount, setSelectedGtmAccount, className }) => (
  <div className={className}>
    {
      gtmAccounts.length ?
        gtmAccounts.map(account =>
          <GtmAccount {...account}
            key={account.accountId}
            isSelected={account.accountId === selectedGtmAccount.accountId}
            setSelectedGtmAccount={setSelectedGtmAccount}
          />) :
        null
    }
    </div>
);

const StyledGtmAccounts = styled(GtmAccounts)`
  max-width: 100%;
  display: flex;
  padding: .7em 2em;
  background-color: rgb(240, 240, 240);
  box-shadow: 1px 1px 1px 1px rgb(200, 200, 200);

  .gtm-account {
    cursor: default;
    padding-right: 1em;
  }
`;

export default StyledGtmAccounts;