import React from 'react';
import styled from 'styled-components';

const Header = ({ children, className }) => (
  <header className={className}>
    <span className="title">GTM MANAGER</span>
    {children}
  </header>
)

const StyledHeader = styled(Header)`
  max-width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1em 2em;
  background-color: rgb(200, 200, 200);

  .title {
    font-weight: bold;
    font-size: 25px;
  }
`;

export default StyledHeader;