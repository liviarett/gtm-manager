import React from 'react';
import styled from 'styled-components';

const statusToIconMap = {
  'ATTENTION': {
    className: 'fas fa-exclamation-circle',
    color: 'darkred',
  },
  'CHECKING': {
    className: 'fas fa-hourglass-half spinning',
    color: 'steelblue',
  },
  'UPDATED': {
    className: 'fas fa-check-circle',
    color: 'darkgreen',
  },
  'UPDATING': {
    className: 'fas fa-cog spinning',
    color: 'orange',
  },
  'FAILED': {
    className: 'fas fa-redo-alt',
    color: 'yellowgreen',
  }
}

const StyledIcon = styled.i`
  color: ${props => props.status ? statusToIconMap[props.status].color : ''};

  &.spinning {
    animation: spin 1s infinite steps(10);
  }

  @-webkit-keyframes spin {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(359deg);
      transform: rotate(359deg);
    }
  }
  @keyframes spin {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(359deg);
      transform: rotate(359deg);
    }
  }
`;

const StatusIcon = ({ status }) => (
  status && statusToIconMap[status]
    ? <StyledIcon status={status} className={statusToIconMap[status].className}></StyledIcon>
  : null
);

export default StatusIcon;