import React from 'react';
import styled from 'styled-components';
import {
  FullWidth,
  Title,
} from '../styles';
import Parameters from './Parameters';

const Flex = styled.span`
  flex-basis: ${props => props.flexWidth};
`;

const Filter = ({ type, parameter, containerId }) => (
  <FullWidth>
    <Flex flexWidth="30%">
      <Title>type:</Title> {type}
    </Flex>
    <Flex flexWidth="70%">
      <Parameters parameters={parameter} containerId={containerId} />
    </Flex>
  </FullWidth>
);

export default Filter;