import React from 'react';
import {
  StyledFiltersContainer,
} from '../styles';
import Filter from './Filter';

const Filters = ({ filters, containerId, triggerId }) => (
  <StyledFiltersContainer>
    {
      filters.map((filter, index) =>
        <Filter
          {...filter}
          containerId={containerId}
          key={`${containerId}-${triggerId}-${index}`}
        />
      )
    }
  </StyledFiltersContainer>
);

export default Filters;