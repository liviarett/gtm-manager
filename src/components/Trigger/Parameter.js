import React from 'react';
import styled from 'styled-components';
import {
  Title,
} from '../styles';

const StyledParameter = styled.span`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
`;

const Parameter = ({ parameter }) => (
  <StyledParameter>
    <span> <Title>key:</Title> {parameter.key} </span>
    <span> <Title>type:</Title> {parameter.type} </span>
    <span> <Title>value:</Title> {parameter.value} </span>
  </StyledParameter>
);

export default Parameter;