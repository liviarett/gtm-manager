import React, { Fragment } from 'react';
import styled from 'styled-components';
import {
  Title,
} from '../styles';
import Parameter from './Parameter';

const StyledParameters = styled.span`
  display: inline-grid;
  margin-left: .3em;
  flex-direction: column;
`;

const Parameters = ({ parameters, containerId }) => (
  <Fragment>
    <Title>parameter:</Title>
    <StyledParameters>
      {
        parameters.map(parameter =>
          <Parameter parameter={parameter} key={`${containerId}-${parameter.key}-${parameter.value}`} />
        )
      }
    </StyledParameters>
  </Fragment>
);

export default Parameters;