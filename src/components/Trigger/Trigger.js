import React from 'react';
import {
  FullWidth,
  SmallFont,
} from '../styles';
import Filters from './Filters';

const Trigger = ({ name, type, filter, triggerId, containerId, customEventFilter}) => (
  <FullWidth customTrigger={customEventFilter}>
    <span>{name}</span>
    <SmallFont style={{ fontSize: '11px' }}>
      <span>{`{{ ${type} }}`}</span>
      {
        customEventFilter && <span>: "{customEventFilter[0].parameter[1].value}"</span>
      }
    </SmallFont>
      {
        filter && <Filters filters={filter} containerId={containerId} triggerId={triggerId}/>
      }
  </FullWidth>
);

export default Trigger;