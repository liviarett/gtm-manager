import React from 'react';
import Trigger from './Trigger/Trigger';

const Triggers = ({ triggers }) =>
  triggers.map(trigger =>
    <Trigger {...trigger} key={`${trigger.containerId}-${trigger.triggerId}`} />
);

export default Triggers;