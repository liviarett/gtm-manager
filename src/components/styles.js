import styled from 'styled-components';

export const SmallFont = styled.div`
  font-size: 11px;
`;

export const FullWidth = styled.div`
  max-width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: .5em 2em;
  flex-wrap: wrap;
  background-color: ${props => props.customTrigger ? 'aliceblue' : 'white'};

  * {
    background-color: transparent;
  }
`;

export const StyledGtmContainer = styled(FullWidth)`
  box-shadow: 0px 1px 1px 0px rgb(235,235,235), 0px -1px 1px 0px rgb(235,235,235);
  padding: 1em 2em;
  background-color: rgb(245,245,245);
  cursor: default;

  span {
    span {
      margin-left: 2em;
    }
  }

  i {
    position: absolute;
    right: .5em;
  }
`;

export const StyledFiltersContainer = styled(SmallFont)`
  font-size: 11px;
  text-align: left;
  padding-top: .3em;
  line-height: 1.5;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const Title = styled.span`
  font-weight: 700;
`;

export const ErrorMessage = styled.div`
  background-color: red;
  position: absolute;
  bottom: 1em;
  right: 1em;
  animation-name: disappear;
  animation-duration: 4s;
  animation-delay: 5s;

  @keyframes disappear {
    0% {
      opacity: 100%;
    }
    100% {
      opacity: 0%;
    }
  }
`;