import gtm from '../api/gtm';
import {
  legacyPageViewTriggerTemplate,
  legacyPageViewHtmlTagTemplate,
} from '../api/resources';
import {
  getLegacyPageviewTrigger,
  getPageViewTriggers,
  getPageViewAndCustomTriggers,
  getCustomTriggers,
} from './triggers.helpers';
import { log } from './helpers';

const checkMap = {
  'hasLegacyTrigger': {
    successMessage: 'Container has legacy trigger!',
    failureMessage: 'Container does not have legacy trigger.',
  },
  'hasLegacyTag': {
    successMessage: 'Container has legacy tag!',
    failureMessage: 'Container does not have legacy tag.',
  },
  'hasUpdatedCustomTriggers': {
    successMessage: 'Container has updated custom triggers!',
    failureMessage: 'Container does not have all updated custom triggers.',
  },
  'hasVersion': {
    successMessage: 'Version has already been created!',
    failureMessage: 'Version has not been created yet.',
  },
  'hasBeenPublished': {
    successMessage: 'Version has already been published!',
    failureMessage: 'Version has not been published yet.',
  }
};

const RETRY_DELAY = 1000;
const CREATE_VERSION_DELAY = 1000;

export const getLocalGtmContainersStatus = () => JSON.parse(window.localStorage.getItem('gtmContainersStatus')) || {};

export const setLocalGtmContainerStatus = (id, status) =>
  window.localStorage.setItem('gtmContainersStatus', JSON.stringify({...getLocalGtmContainersStatus(), [id]: status }))

export const getLegacyTag = (legacyTagTemplate, tags) =>
  tags.filter(tag => tag.name === legacyTagTemplate.name
    && (!!tag.firingTriggerId && tag.firingTriggerId[0] === legacyTagTemplate.firingTriggerId[0]));

export const versionHasLegacyTrigger = (versionTriggers, legacyTrigger) =>
  versionTriggers.find(versionTrigger => versionTrigger.name === legacyTrigger.name && versionTrigger.type === legacyTrigger.type);

export const versionHasLegacyTag = (versionTags, legacyTag) =>
  versionTags.find(versionTag => versionTag.name === legacyTag.name && versionTag.type === legacyTag.type);

export const versionHasCustomTriggers = (versionTriggers, customTriggers) =>
  customTriggers.every(trigger => versionTriggers.find(versionTrigger => versionTrigger.triggerId === trigger.triggerId && versionTrigger.name === trigger.name))

export const versionHasBeenPublished = (latestVersion, liveVersion) =>
  latestVersion.containerVersionId === liveVersion.containerVersionId

export const createLegacyTrigger = (gtmStatus, triggers, path) =>
  new Promise((resolve, reject) => {
    if (gtmStatus.hasLegacyTrigger) {
      const legacyTrigger = getLegacyPageviewTrigger(triggers)[0];

      resolve(legacyTrigger);
    } else {
      log('Creating legacy trigger...')
      resolve(gtm.createTrigger(path, legacyPageViewTriggerTemplate))
    }
  });

export const createLegacyTag = (gtmStatus, trigger, path) =>
  new Promise((resolve, reject) => {
    if (gtmStatus.hasLegacyTag) {
      resolve();
    } else if (!trigger || !trigger.triggerId) {
      reject(new Error('No trigger id'));
    } else {
      const legacyTagTemplate = legacyPageViewHtmlTagTemplate(trigger.triggerId);

      log('Creating legacy tag...')
      const result = gtm.createTag(path, legacyTagTemplate);

      resolve(result);
    }
  });

  //TODO: NEEDS REFACTORING
export const updateTriggers = (gtmStatus, triggers) =>
  new Promise((resolve, reject) => {
  if (gtmStatus.hasUpdatedCustomTriggers) {
    resolve();
  } else {
    let index = 0;
    const filteredTriggers = getPageViewTriggers(triggers);

    const retryUpdateTrigger = (trigger) => {
      log('Update failed, trying again...');
      return setTimeout(() => {
        updateTrigger(trigger);
      }, RETRY_DELAY);
    }

    const updateTrigger = (trigger) => new Promise((resolve, reject) => {
      log(`[${index + 1}/${filteredTriggers.length}] Updating trigger: ${filteredTriggers[index].name}...`);
      resolve(gtm.updateTrigger(trigger))
    })
      .then((result) => {
        if (result.status === 200 && filteredTriggers[index + 1]) {
          index++;
          updateTrigger(filteredTriggers[index]);
        } else if (result.status !== 200) {
          retryUpdateTrigger(filteredTriggers[index]);
        } else {
          log(`${index + 1} triggers updated!`);
          resolve();
        }
      })
      .catch((e) => {
        retryUpdateTrigger(filteredTriggers[index]);
      });

    return filteredTriggers.length && updateTrigger(filteredTriggers[index])
  }
});

export const createNewVersion = (gtmStatus, latestVersion, path) =>
  new Promise((resolve, reject) => {
    if (gtmStatus.hasVersion) {
      resolve(latestVersion);
    } else {
      log('Creating new version...');
      setTimeout(() => {
        resolve(gtm.createNewVersion(path));
      }, CREATE_VERSION_DELAY)
    }
  })

export const publishVersion = (gtmStatus, version) =>
  new Promise((resolve, reject) => {
    if (gtmStatus.hasBeenPublished) {
      resolve();
    } else if (!version || !version.path) {
      reject(new Error('No version found'));
    } else {
      log(`Publishing version ${version.containerVersionId}...`);
      resolve(gtm.publishVersion(version.path))
    }
  });

export const updateGtmContainer = ({ workspaces, publicId, triggers, gtmStatus, latestVersion }) =>
  new Promise((resolve, reject) => {
    const workspacePath = workspaces[0].path;

    log(`-- Updating Container ${publicId}`);

    createLegacyTrigger(gtmStatus, triggers, workspacePath)
      .then(trigger => createLegacyTag(gtmStatus, trigger, workspacePath))
      .then(() => updateTriggers(gtmStatus, triggers))
      .then(() => createNewVersion(gtmStatus, latestVersion, workspacePath))
      .then(version => publishVersion(gtmStatus, version))
      .then(status => resolve(status))
      .catch((e) => {
        log(e);
        resolve('FAILED');
      })
  });

const getWorkspacePath = (workspaces) => workspaces && workspaces[0] && workspaces[0].path;

const resolveCheck = (callback) => (statusKey, condition, successResult) =>
  new Promise((resolve, reject) => {
    if (condition) {
      callback(statusKey, true);
      log(checkMap[statusKey].successMessage);
      resolve(successResult);
    } else {
      callback(statusKey, false);
      reject(checkMap[statusKey].failureMessage);
    }
  });

const checkLegacyTrigger = (triggers, check) =>
  new Promise((resolve, reject) => {
    const legacyPageViewTrigger = getLegacyPageviewTrigger(triggers) && getLegacyPageviewTrigger(triggers)[0];
    const containerHasCreatedLegacyTrigger = !!legacyPageViewTrigger;

    resolve(check('hasLegacyTrigger', containerHasCreatedLegacyTrigger, legacyPageViewTrigger))
  });

const checkLegacyTag = (workspacePath, legacyPageViewTrigger, updateContainer, check) =>
  new Promise(async (resolve, reject) => {
    const tags = await gtm.getTags(workspacePath, updateContainer);

    const legacyTagTemplate = legacyPageViewHtmlTagTemplate(legacyPageViewTrigger.triggerId);
    const containerHasLegacyTag =
      !!getLegacyTag(legacyTagTemplate, tags).length;

    resolve(check('hasLegacyTag', containerHasLegacyTag))
  });

const checkCustomTriggers = (triggers, check) =>
  new Promise((resolve, reject) => {
    const containerHasUpdatedAllPageviewTriggers = !getPageViewTriggers(triggers).length && !!getCustomTriggers(triggers).length;

    resolve(check('hasUpdatedCustomTriggers', containerHasUpdatedAllPageviewTriggers))
  });

const checkLatestVersion = (triggers, path, updateContainer, check) =>
  new Promise(async (resolve, reject) => {
    const version = await gtm.getLatestVersion(path, updateContainer);

    const legacyPageViewTrigger = getLegacyPageviewTrigger(triggers) && getLegacyPageviewTrigger(triggers)[0];
    const legacyTagTemplate = legacyPageViewHtmlTagTemplate(legacyPageViewTrigger.triggerId);

    const hasLegacyTrigger = !!versionHasLegacyTrigger(version.trigger, legacyPageViewTrigger);
    const hasLegacyTag = !!versionHasLegacyTag(version.tag, legacyTagTemplate);
    const hasCustomTriggers = !!versionHasCustomTriggers(version.trigger, getCustomTriggers(triggers));

    const versionHasBeenCreated = hasLegacyTrigger && hasLegacyTag && hasCustomTriggers;

    resolve(check('hasVersion', versionHasBeenCreated, version))
  });

const checkLiveVersion = (version, path, updateContainer, check) =>
  new Promise(async (resolve, reject) => {
    const liveVersion = await gtm.getLiveVersion(path, updateContainer);
    const hasBeenPublished = versionHasBeenPublished(version, liveVersion);

    resolve(check('hasBeenPublished', hasBeenPublished))
  });

const handleException = (message, triggers, callback) =>
  new Promise((resolve, reject) => {
    log(message);
    callback({
      filteredTriggers: getPageViewTriggers(triggers),
      status: 'ATTENTION',
    });
    resolve();
  });

export const checkGtmContainer = (component) =>
  new Promise(async (resolve, reject) => {
    const {
      props,
      getTriggers,
      updateContainer,
      setContainerUpdated,
      updateContainerStatus,
      updateContainerGtmStatus
    } = component;
    const { publicId, path } = props;
    log(`-- Checking Container ${publicId}`)
    updateContainerStatus('CHECKING');

    let triggers;
    try {
      triggers = await getTriggers();
    } catch {
      return updateContainerStatus('FAILED');
    }

    const workspacePath = () => getWorkspacePath(component.props.workspaces);
    const check = resolveCheck(updateContainerGtmStatus);
    const containerHasTargetedTriggers = getPageViewAndCustomTriggers(triggers).length;

    if (!containerHasTargetedTriggers) {
      log('Container does not need updating!');
      setContainerUpdated();
      return resolve();
    }

    return checkLegacyTrigger(triggers, check)
      .then((trigger) => checkLegacyTag(workspacePath(), trigger, updateContainer, check))
      .then(() => checkCustomTriggers(triggers, check))
      .then(() => checkLatestVersion(triggers, path, updateContainer, check))
      .then((version) => checkLiveVersion(version, path, updateContainer, check))
      .then(() => {
        setContainerUpdated();
        resolve();
      })
      .catch((message) => resolve(handleException(message, triggers, updateContainer)))
  });