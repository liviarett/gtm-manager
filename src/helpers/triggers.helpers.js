
import { legacyPageViewTriggerTemplate } from '../api/resources';

const matchMethods = ['matchRegex', 'contains', 'urlMatches'];

const CUSTOM_TRIGGER_VALUE = 'webform_submission_completed';
const NEGATE_TRIGGER = {
  key: 'negate',
  value: 'true',
}
const PAGEVIEW_TRIGGER = {
  type: 'pageview',
  value: 'sid=',
}

export const getLegacyPageviewTrigger = (triggers) =>
  triggers
    .filter(trigger =>
      trigger.type === legacyPageViewTriggerTemplate.type &&
      trigger.name === legacyPageViewTriggerTemplate.name
    )

export const getPageViewTriggers = (triggers) =>
triggers &&
  triggers
    .filter(trigger =>
      trigger.type === PAGEVIEW_TRIGGER.type &&
      trigger.name !== legacyPageViewTriggerTemplate.name &&
      trigger.filter &&
      trigger.filter
      .some(f => {
        return matchMethods.includes(f.type) && f.parameter.some(p => p.value === PAGEVIEW_TRIGGER.value);
      }) &&
      trigger.filter
      .every(f => f.parameter.every(p => !(p.key === NEGATE_TRIGGER.key && p.value === NEGATE_TRIGGER.value)))
    )

export const getCustomTriggers = (triggers) =>
  triggers.filter(trigger => !!trigger.customEventFilter
    && trigger.customEventFilter.some(filter => filter.parameter.some(p => p.value === CUSTOM_TRIGGER_VALUE)))

export const getPageViewAndCustomTriggers = (triggers) =>
  triggers && getPageViewTriggers(triggers)
      .concat(getCustomTriggers(triggers))