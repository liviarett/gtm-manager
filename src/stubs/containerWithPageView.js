export default {
  path: 'accounts/4701777440/containers/11702415',
  accountId: '4701777440',
  containerId: '11702415',
  name: 'Test Container',
  publicId: 'GTM-M9WZT58',
  usageContext: [
    'web'
  ],
  gtmStatus: {
    hasLegacyTrigger: false,
  },
  fingerprint: '1556365576533',
  tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces?apiLink=container',
  status: 'CHECKING',
  workspaces: [
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      name: 'Default Workspace',
      fingerprint: '1556365576533',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120?apiLink=workspace'
    }
  ],
  triggers: [
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/5',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '5',
      name: 'Biddable All Pages',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'cid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556018964940',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/5?apiLink=trigger'
    },
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/7',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '7',
      name: 'Biddable Page View - Hearing FR',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'auditive'
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        },
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'sid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            },
            {
              type: 'boolean',
              key: 'negate',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556018964941',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/7?apiLink=trigger'
    },
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/8',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '8',
      name: 'HA FR Form Page Non Converter',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'aides-auditives'
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        },
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'sid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            },
            {
              type: 'boolean',
              key: 'negate',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556018964942',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/8?apiLink=trigger'
    },
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/121',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '121',
      name: 'Biddable Conversion',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'sid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        },
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'aides-auditives'
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556365576476',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/121?apiLink=trigger'
    },
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/122',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '122',
      name: 'Hearing Aid FR Conversion',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'sid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        },
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'auditives'
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556365576479',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/122?apiLink=trigger'
    }
  ],
  filteredTriggers: [
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/5',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '5',
      name: 'Biddable All Pages',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'cid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556018964940',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/5?apiLink=trigger'
    },
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/7',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '7',
      name: 'Biddable Page View - Hearing FR',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'auditive'
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        },
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'sid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            },
            {
              type: 'boolean',
              key: 'negate',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556018964941',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/7?apiLink=trigger'
    },
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/8',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '8',
      name: 'HA FR Form Page Non Converter',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'aides-auditives'
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        },
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'sid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            },
            {
              type: 'boolean',
              key: 'negate',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556018964942',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/8?apiLink=trigger'
    },
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/121',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '121',
      name: 'Biddable Conversion',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'sid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        },
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'aides-auditives'
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556365576476',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/121?apiLink=trigger'
    },
    {
      path: 'accounts/4701777440/containers/11702415/workspaces/120/triggers/122',
      accountId: '4701777440',
      containerId: '11702415',
      workspaceId: '120',
      triggerId: '122',
      name: 'Hearing Aid FR Conversion',
      type: 'pageview',
      filter: [
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'sid='
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        },
        {
          type: 'matchRegex',
          parameter: [
            {
              type: 'template',
              key: 'arg0',
              value: '{{Page URL}}'
            },
            {
              type: 'template',
              key: 'arg1',
              value: 'auditives'
            },
            {
              type: 'boolean',
              key: 'ignore_case',
              value: 'true'
            }
          ]
        }
      ],
      fingerprint: '1556365576479',
      tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/120/triggers/122?apiLink=trigger'
    }
  ]
}