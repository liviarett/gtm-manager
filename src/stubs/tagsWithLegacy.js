export default triggerId => ([
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/3',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '3',
    name: 'AdWords Remarketing - Laser Eye FR',
    type: 'sp',
    parameter: [
      {
        type: 'template',
        key: 'conversionId',
        value: '859682951'
      },
      {
        type: 'template',
        key: 'customParamsFormat',
        value: 'NONE'
      },
      {
        type: 'template',
        key: 'conversionLabel',
        value: 'OzEMCKrv2G4Qh_H2mQM'
      }
    ],
    fingerprint: '1556018964946',
    firingTriggerId: [
      '5'
    ],
    tagFiringOption: 'oncePerEvent',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/3?apiLink=tag'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/7',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '7',
    name: 'Conversion Linker',
    type: 'gclidw',
    parameter: [
      {
        type: 'boolean',
        key: 'enableCookieOverrides',
        value: 'false'
      }
    ],
    fingerprint: '1556018964949',
    firingTriggerId: [
      '2147479553'
    ],
    tagFiringOption: 'oncePerEvent',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/7?apiLink=tag'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/8',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '8',
    name: 'Facebook Healthcare All Pages Pixel',
    type: 'html',
    parameter: [
      {
        type: 'template',
        key: 'html',
        value: '<!-- Facebook Pixel Code -->\n<script>\n!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?\nn.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;\nn.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;\nt.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,\ndocument,\'script\',\'https://connect.facebook.net/en_US/fbevents.js\');\n\nfbq(\'init\', \'431902023625843\');\nfbq(\'track\', "PageView");</script>\n<noscript><img height="1" width="1" style="display:none"\nsrc="https://www.facebook.com/tr?id=431902023625843&ev=PageView&noscript=1"\n/></noscript>\n<!-- End Facebook Pixel Code -->'
      },
      {
        type: 'boolean',
        key: 'supportDocumentWrite',
        value: 'false'
      }
    ],
    fingerprint: '1556018964949',
    firingTriggerId: [
      '2147479553'
    ],
    tagFiringOption: 'oncePerEvent',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/8?apiLink=tag'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/13',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '13',
    name: 'Nanigans Landing',
    type: 'html',
    parameter: [
      {
        type: 'template',
        key: 'html',
        value: '<script>\n  // NaN_api = [[app_id, user_id], [type, name, value, extra]];\n  NaN_api = [\n    [[75349], \'{{c_user}}\'],\n    [\'visit\', \'landing\']\n  ]; \n\n  (function() {\n    var s = document.createElement(\'script\');\n    s.async = true;\n    s.src = \'//cdn.nanigans.com/NaN_tracker.js\';\n    var h = document.getElementsByTagName(\'head\')[0];\n    h.appendChild(s);\n  })();\n</script>\n\n<noscript><img style="display: none;" src="//api.nanigans.com/event.php?app_id=75349&type=visit&name=landing&user={{c_user}}"></noscript>'
      },
      {
        type: 'boolean',
        key: 'supportDocumentWrite',
        value: 'true'
      }
    ],
    fingerprint: '1556018964955',
    firingTriggerId: [
      '5'
    ],
    tagFiringOption: 'oncePerEvent',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/13?apiLink=tag'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/14',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '14',
    name: 'Outbrain - CC - Hearing Aids FR Conversion',
    type: 'html',
    parameter: [
      {
        type: 'template',
        key: 'html',
        value: '<script data-obct type="text/javascript">\n  /** DO NOT MODIFY THIS CODE**/\n  !function(_window, _document) {\n    var OB_ADV_ID=\'00b9b9e31fc55d72571f98f51a4bc5afb8\';\n    if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === \'[object Array]\' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}\n    var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = \'1.1\';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement(\'script\');tag.async = true;tag.src = \'//amplify.outbrain.com/cp/obtp.js\';tag.type = \'text/javascript\';var script = _document.getElementsByTagName(\'script\')[0];script.parentNode.insertBefore(tag, script);}(window, document);\nobApi(\'track\', \'PAGE_VIEW\');\n</script>\n\n'
      },
      {
        type: 'boolean',
        key: 'supportDocumentWrite',
        value: 'false'
      }
    ],
    fingerprint: '1556018964955',
    firingTriggerId: [
      '2147479553'
    ],
    tagFiringOption: 'oncePerEvent',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/14?apiLink=tag'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/19',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '19',
    name: 'Twitter - Site Visits',
    type: 'html',
    parameter: [
      {
        type: 'template',
        key: 'html',
        value: '<!-- Twitter single-event website tag code -->\n<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>\n<script type="text/javascript">twttr.conversion.trackPid(\'nw332\', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>\n<noscript>\n<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nw332&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />\n<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nw332&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />\n</noscript>\n<!-- End Twitter single-event website tag code -->'
      },
      {
        type: 'boolean',
        key: 'supportDocumentWrite',
        value: 'true'
      }
    ],
    fingerprint: '1556018964959',
    firingTriggerId: [
      '5'
    ],
    tagFiringOption: 'oncePerEvent',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/19?apiLink=tag'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/20',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '20',
    name: 'Universal Analytics - devis.cliniccompare.fr',
    type: 'ua',
    parameter: [
      {
        type: 'boolean',
        key: 'overrideGaSettings',
        value: 'true'
      },
      {
        type: 'boolean',
        key: 'doubleClick',
        value: 'false'
      },
      {
        type: 'boolean',
        key: 'setTrackerName',
        value: 'false'
      },
      {
        type: 'boolean',
        key: 'useDebugVersion',
        value: 'false'
      },
      {
        type: 'boolean',
        key: 'useHashAutoLink',
        value: 'false'
      },
      {
        type: 'template',
        key: 'trackType',
        value: 'TRACK_PAGEVIEW'
      },
      {
        type: 'boolean',
        key: 'decorateFormsAutoLink',
        value: 'false'
      },
      {
        type: 'boolean',
        key: 'enableLinkId',
        value: 'false'
      },
      {
        type: 'boolean',
        key: 'enableEcommerce',
        value: 'false'
      },
      {
        type: 'template',
        key: 'trackingId',
        value: 'UA-9496345-29'
      }
    ],
    fingerprint: '1556018964959',
    firingTriggerId: [
      '2147479553'
    ],
    tagFiringOption: 'oncePerEvent',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/20?apiLink=tag'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/302',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '302',
    name: 'AdWords - Conversion - Hearing FR',
    type: 'awct',
    parameter: [
      {
        type: 'template',
        key: 'conversionId',
        value: '852017363'
      },
      {
        type: 'template',
        key: 'conversionLabel',
        value: 'YqawCMeKmnEQ04GjlgM'
      }
    ],
    fingerprint: '1556541221856',
    firingTriggerId: [
      '139'
    ],
    tagFiringOption: 'oncePerEvent',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/302?apiLink=tag'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/141/tags/313',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '141',
    tagId: '313',
    name: 'Legacy Conversion Page',
    type: 'html',
    parameter: [
      {
        type: 'template',
        key: 'html',
        value: '<script>dataLayer.push({ event: "webform_submission_completed" })</script>'
      },
      {
        type: 'boolean',
        key: 'supportDocumentWrite',
        value: 'true'
      }
    ],
    fingerprint: '1556541261167',
    firingTriggerId: [
      triggerId
    ],
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/141/tags/313?apiLink=tag'
  }
]);