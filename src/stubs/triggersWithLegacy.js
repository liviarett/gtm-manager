const triggers = [
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/116/triggers/5',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '116',
    triggerId: '5',
    name: 'Biddable All Pages',
    type: 'pageview',
    filter: [
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'cid='
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          }
        ]
      }
    ],
    fingerprint: '1556018964940',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/116/triggers/5?apiLink=trigger'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/116/triggers/7',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '116',
    triggerId: '7',
    name: 'Biddable Page View - Hearing FR',
    type: 'pageview',
    filter: [
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'auditive'
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          }
        ]
      },
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'sid='
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          },
          {
            type: 'boolean',
            key: 'negate',
            value: 'true'
          }
        ]
      }
    ],
    fingerprint: '1556018964941',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/116/triggers/7?apiLink=trigger'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/116/triggers/8',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '116',
    triggerId: '8',
    name: 'HA FR Form Page Non Converter',
    type: 'pageview',
    filter: [
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'aides-auditives'
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          }
        ]
      },
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'sid='
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          },
          {
            type: 'boolean',
            key: 'negate',
            value: 'true'
          }
        ]
      }
    ],
    fingerprint: '1556018964942',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/116/triggers/8?apiLink=trigger'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/116/triggers/117',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '116',
    triggerId: '117',
    name: 'Biddable Conversion',
    type: 'pageview',
    filter: [
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'sid='
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          }
        ]
      },
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'aides-auditives'
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          }
        ]
      }
    ],
    fingerprint: '1556325901874',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/116/triggers/117?apiLink=trigger'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/116/triggers/118',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '116',
    triggerId: '118',
    name: 'Hearing Aid FR Conversion',
    type: 'pageview',
    filter: [
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'sid='
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          }
        ]
      },
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'auditives'
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          }
        ]
      }
    ],
    fingerprint: '1556325901877',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/116/triggers/118?apiLink=trigger'
  },
  {
    path: 'accounts/4701777440/containers/11702415/workspaces/116/triggers/119',
    accountId: '4701777440',
    containerId: '11702415',
    workspaceId: '116',
    triggerId: '119',
    name: 'Legacy Conversion Page View Trigger',
    type: 'pageview',
    filter: [
      {
        type: 'matchRegex',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'sid='
          },
          {
            type: 'boolean',
            key: 'ignore_case',
            value: 'true'
          }
        ]
      }
    ],
    fingerprint: '1556326068647',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces/116/triggers/119?apiLink=trigger'
  }
]

export default triggers;