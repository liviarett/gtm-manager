export default [
  {
    path: 'accounts/31828079/containers/1164473/workspaces/15/triggers/1',
    accountId: '31828079',
    containerId: '1164473',
    workspaceId: '15',
    triggerId: '1',
    name: 'PPC All Pages',
    type: 'pageview',
    filter: [
      {
        type: 'contains',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'www.centrumpruzkumu.cz'
          }
        ]
      }
    ],
    fingerprint: '1430996408065',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/31828079/containers/1164473/workspaces/15/triggers/1?apiLink=trigger'
  },
  {
    path: 'accounts/31828079/containers/1164473/workspaces/15/triggers/2',
    accountId: '31828079',
    containerId: '1164473',
    workspaceId: '15',
    triggerId: '2',
    name: 'PPC Conversion Page',
    type: 'pageview',
    filter: [
      {
        type: 'contains',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'www.centrumpruzkumu.cz/d%C4%9Bkuji'
          }
        ]
      }
    ],
    fingerprint: '1431337704329',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/31828079/containers/1164473/workspaces/15/triggers/2?apiLink=trigger'
  },
  {
    path: 'accounts/31828079/containers/1164473/workspaces/15/triggers/3',
    accountId: '31828079',
    containerId: '1164473',
    workspaceId: '15',
    triggerId: '3',
    name: 'Custom Event',
    type: 'customEvent',
    customEventFilter: [
      {
        type: 'equals',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{_event}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'GAEvent'
          }
        ]
      }
    ],
    fingerprint: '1433863764077',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/31828079/containers/1164473/workspaces/15/triggers/3?apiLink=trigger'
  },
  {
    path: 'accounts/31828079/containers/1164473/workspaces/15/triggers/11',
    accountId: '31828079',
    containerId: '1164473',
    workspaceId: '15',
    triggerId: '11',
    name: 'Biddable Non Thankyou',
    type: 'pageview',
    filter: [
      {
        type: 'contains',
        parameter: [
          {
            type: 'template',
            key: 'arg0',
            value: '{{Page URL}}'
          },
          {
            type: 'template',
            key: 'arg1',
            value: 'd%C4%9Bkuji'
          },
          {
            type: 'boolean',
            key: 'negate',
            value: 'true'
          }
        ]
      }
    ],
    fingerprint: '1506339458862',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/31828079/containers/1164473/workspaces/15/triggers/11?apiLink=trigger'
  }
]