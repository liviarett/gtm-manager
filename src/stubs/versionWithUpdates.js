export default (tag, trigger, containerVersionId = '52') => ({
  path: 'accounts/4701777440/containers/11702415/versions/52',
  accountId: '4701777440',
  containerId: '11702415',
  containerVersionId,
  name: 'Custom Conversion Events',
  description: 'This version updates converion triggers to listen for the custom event webform_submission_completed and creates a backwards compatible html tag that injects that event on the legacy conversion page load',
  container: {
    path: 'accounts/4701777440/containers/11702415',
    accountId: '4701777440',
    containerId: '11702415',
    name: 'Test Container',
    publicId: 'GTM-M9WZT58',
    usageContext: [
      'web'
    ],
    fingerprint: '1556541261167',
    tagManagerUrl: 'https://tagmanager.google.com/#/container/accounts/4701777440/containers/11702415/workspaces?apiLink=container'
  },
  tag,
  trigger,
  variable: [
    {
      accountId: '4701777440',
      containerId: '11702415',
      variableId: '1',
      name: 'c_user',
      type: 'k',
      parameter: [
        {
          type: 'boolean',
          key: 'decodeCookie',
          value: 'false'
        },
        {
          type: 'template',
          key: 'name',
          value: 'c_user'
        }
      ],
      fingerprint: '1556018964943'
    },
    {
      accountId: '4701777440',
      containerId: '11702415',
      variableId: '2',
      name: 'nanigans_sid',
      type: 'u',
      parameter: [
        {
          type: 'template',
          key: 'component',
          value: 'QUERY'
        },
        {
          type: 'template',
          key: 'queryKey',
          value: 'sid'
        }
      ],
      fingerprint: '1556018964944'
    }
  ],
  builtInVariable: [
    {
      accountId: '4701777440',
      containerId: '11702415',
      type: 'pageUrl',
      name: 'Page URL'
    },
    {
      accountId: '4701777440',
      containerId: '11702415',
      type: 'pageHostname',
      name: 'Page Hostname'
    },
    {
      accountId: '4701777440',
      containerId: '11702415',
      type: 'pagePath',
      name: 'Page Path'
    },
    {
      accountId: '4701777440',
      containerId: '11702415',
      type: 'referrer',
      name: 'Referrer'
    },
    {
      accountId: '4701777440',
      containerId: '11702415',
      type: 'event',
      name: 'Event'
    }
  ],
  fingerprint: '1556541353690',
  tagManagerUrl: 'https://tagmanager.google.com/#/versions/accounts/4701777440/containers/11702415/versions/52?apiLink=version'
})